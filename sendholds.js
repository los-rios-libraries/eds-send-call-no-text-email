var college = window.parent.document.getElementById('collegeID').innerHTML; // defined in bottom branding
var serverPath = 'https://www.library.losrios.edu/resources/onesearch/detailed-record/send-holdings/';
var isCatRec = window.parent.document.querySelector('.record-has-rtac');
function createSendIframe(item, row, perm)
{
  var holdingsArea = window.parent.document.querySelector('.record-has-rtac');
  var holdsendFrameContainer = window.parent.document.getElementById('lr-frame-container');
  var createFrameMarkup = '<div id="sendframe" tabindex="1"><div class="lr-frame-closer"> <button id="lr-frame-close-but" type="button" aria-label="close" title="close"> X </button></div><h2 tabindex="0" id="frame-heading"><img alt="share" class="icon" src="' + serverPath + 'img/share.png"> Send this Call Number</h2><iframe id="send-holdings" width="100%" height="400" src="' + serverPath + 'index.php?title=' + item + '&holding=' + row + '&college=' + college + '&an=ep.uid"></iframe></div>';
  if (holdsendFrameContainer === null)
  {
    holdsendFrameContainer = window.parent.document.createElement('div');
    holdsendFrameContainer.id = 'lr-frame-container';
    holdsendFrameContainer.setAttribute('aria-live', 'polite');
    holdsendFrameContainer.innerHTML = createFrameMarkup;
    holdingsArea.appendChild(holdsendFrameContainer);
  }
  else
  {
    holdsendFrameContainer.innerHTML = createFrameMarkup;
  }
  window.parent.document.getElementById('lr-frame-close-but').addEventListener('click',function(e) {
    e.preventDefault();
    closeSendIframe();
    window.parent.document.ga('send', 'event', 'rtac', 'close', 'send call no window' );
  });
  window.parent.document.getElementById('send-holdings').focus();
}

function closeSendIframe()
{
  var holdsendFrameContainer = window.parent.document.getElementById('lr-frame-container');
  holdsendFrameContainer.parentNode.removeChild(holdsendFrameContainer);
}
// first get text from table
var rtacRows = window.parent.document.querySelectorAll('.rtac-table tbody tr');
// get item title and make it URL-friendly
function detRecLinks()
{
  var itemTitle = window.parent.document.querySelector('.citation-title').textContent; // can't use EBSCO's ep.Title string because of special characters
itemTitle = itemTitle.replace(/\s/g, '+'); // get rid of extra white space
itemTitle = itemTitle.replace(/['"]/g, '&#8217;'); // special characters...
var framePath = 'document.querySelector(\'#SendHoldings + div div iframe\').contentWindow';
  //get the holdings text from each row and strip out the extra spaces
  for (var i = 0; i < rtacRows.length; i++)
  {
    var holdingsInfo = rtacRows[i].textContent;
    var holdingsTrimmed = holdingsInfo.replace(/[\t'"]/g, '');
    holdingsTrimmed = holdingsTrimmed.replace(/[\s\n]/g, '+');
    holdingsTrimmed = holdingsTrimmed.replace(/\+\+/g, '+');
    holdingsTrimmed = holdingsTrimmed.replace(/#/g, '@'); // this is needed because ARC's reserves have # signs which cannot be read by php - php script will replace @ with #
    var holdingsHTML = rtacRows[i].innerHTML;
    //   var sendPage = serverPath + 'index.php';
    // replace the row with the new HTML
    holdingsAdd = holdingsHTML.replace(/<\/td>(\s)+<td class="status">/, '<span class="text-this"><button type="button" class="email-text-holdings button"  title="Send this call number" onclick="ga(\'send\', \'event\', \'rtac\', \'click\', \'send call number\');' + framePath + '.createSendIframe(\'' + itemTitle + '\',\'' + holdingsTrimmed + '\', \'ep.uid\'); return false;"> <img width="12" height="9" class="lr-save-icon" src="' + serverPath + 'img/share.png" alt="share"></a></div></td><td class="status">');
    for (var j = 0; j < rtacRows.length; j++)
    {
      if (rtacRows[i].innerHTML.indexOf('Show') === -1)
      { // make sure we're not adding links to the "show more" rows
        rtacRows[i].innerHTML = holdingsAdd;
      }
    }
  }
}
// ok do it already
if (isCatRec)
{
  var rtacCall = setInterval(function ()
  {
    if (window.parent.document.querySelector('.rtac-table') !== null)
    {
      console.log('rtac found');
      clearInterval(rtacCall);
      detRecLinks();
    }
  }, 200);
}