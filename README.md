# README #

## NO LONGER UPDATED ##
This has been incorporated into the EDS-Complete repository and any further edits will be done there.

* * *

Allows users of EBSCO Discovery Service to send holdings information via text message (email-to-sms) or email.
## This package includes: ##
* script for Detailed Record widget. Needs to be wrapped in `<script></script>` before saving in an EDS profile.
* styles that should be added to a stylesheet called in via EBSCOhost bottom branding (best if the styles load before the widget does)
* PHP files that are called in via an iframe created by the script
* styles for those PHP files
* phone and email icons (public domain from the web)
## Warning ##
* Anyone adapting this should be aware that it was not created by professional developers. There may be issues and you will need to take care in making changes. There are a few things in it specifically related to our consortial setup.
* You are by no means authorized to use the PHP scripts residing on our servers in your live implementation or even your testing. You should host the PHP files yourself.
## To Do ##
* investigate sendmail headers further
* should this all really be in jQuery
* should it be limited to users who are either on-campus or authenticated
## Screen shots ##
### Send buttons in holdings table: ###
![call number display](https://bitbucket.org/repo/nArab4/images/1686794583-onesearh-share-callno.png)
***

### Form used for sending text messages: ###
![SMS form](https://bitbucket.org/repo/nArab4/images/966384435-sms-form.png)
***

### Text message received: ###
![text message received by phone](//bytebucket.org/los-rios-libraries/eds-send-call-no-text-email/raw/f7cf3df9a1c99adbc586ba68d53e710e93961068/screenshots/text-message.png)