<?php
$college = $_POST['college'];
$email = $_POST['address'];
$title = $_POST['title'];
$location = $_POST['location'];
$callNo = $_POST['callno'];
$number = $_POST['number'];
$domain = $_POST['domain'];
$gateway = $number . '@' . $domain;

$ip = $_SERVER['REMOTE_ADDR'];
$medium = $_POST['medium'];
$perm = urldecode($_POST['perm']);
$errormsg = '<p>Error. You may have left out information or entered it in the wrong format, or the system may be down. Please click the email or text icon and try again. If the error continues, please contact your college library.</p>';

$date = date("Y-M-d");
$time = date("g:i a");
$loginfo = $date . "\t" . $time . "\t" . $college ."\t".$medium ."\t".$location ."\t".$domain . "\n" . $ip . "\n";
$filename = 'http://wserver.scc.losrios.edu/~library/j/onesearch/detailed-record/send-holdings/logs/send-log.tsv';



/*
Doesn't work! can't seem to write to wserver from remote server. Also couldn't set permissions on windows server. Handling via Google Analytics...
$handle = fopen($filename, 'a');
fwrite($handle, $loginfo);
fclose($handle);
*/
switch ($college) {
	case 'arc':
		$collURL = 'http://www.arc.losrios.edu/arclibrary.htm';
		$SMSsender = 'ARC';
		$replyToEmail = 'noreply@arc.losrios.edu';
		$gaNo = '2';
		break;
	case 'crc':
		$collURL = 'http://www.crc.losrios.edu/library';
		$SMSsender = 'CRC';
		$replyToEmail = 'library@crc.losrios.edu';
		$gaNo = '3';
		break;
	case 'flc':
		$collURL = 'http://www.flc.losrios.edu/libraries';
		$SMSsender = 'FLC';
		$replyToEmail = 'FLC-Librarian@flc.losrios.edu';
		$gaNo = '4';
		break;
	case 'scc':
		$collURL = 'http://www.scc.losrios.edu/library';
		$SMSsender = 'SCC';
		$replyToEmail = 'scclibrary@scc.losrios.edu';
		$gaNo = '5';
		break;	
}
$headers = 'From: '. $SMSsender .' Library <' . $replyToEmail . '>';
switch ($medium) {
case ('sms'): 
$msg = $title . "\r\n" . $location . "\r\n\r\n" .$callNo;
$msgShow = '<p>' . $title . '</p><p>'. $location . '</p><p>' .$callNo . '</p>';
$dest = $number;
break;
case ('email'):
$msg = $title . "\r\n\r\n" .$location. "\r\n\r\n" .$callNo . "\r\n\r\n" . 'View this record:' . "\r\n" .$perm . "\r\n\r\n" . $collURL. "\r\n\r\n". 'Sent from IP address: ' .$ip;
$msgShow = '<p>'.$title . '</p><p>' .$holding . '</p><p>View this record: ' .$perm .'</p><p>Sent from IP address: ' .$ip .'</p>';
$dest = $email;
}

$confirm = '<p>The following message was sent to ' .$dest .':</p><div id="message">' .$msgShow .'</div>';
?>
<!DOCTYPE HTML>
<html>
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow">
	<title>
		Confirmation
	</title>
	<link rel="stylesheet" type="text/css" href="style.css">
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-44798235-<?php echo $gaNo; ?>', 'auto');
  ga('send', 'pageview');
  ga('set', 'anonymizeIp', true);
</script>

</head>
<body>
<div id="container">



<?php

if ($medium === 'sms') {
	if ($domain === '') {
		

		echo $errormsg;

	}
	else if (preg_match('/^\d{10}$/', $number)) {


mail($gateway,null,$msg,$headers);

echo $confirm;
echo '<script>ga(\'send\', \'event\', \'send call no\', \'send\', \'sms\');</script>';
echo '<script>ga(\'send\', \'event\', \'sms carrier\', \'choose\', \'' .$domain .'\');</script>';

}
	else {

	echo $errormsg;
	break;

	}

}

else if ($medium === 'email') {
	if (preg_match('/^.*@.*\.[a-z]{2,7}$/i', $email)) {
	mail($email,'call number from ' . $SMSsender .' Library',$msg,$headers);
	echo $confirm;
	echo '<script>ga(\'send\', \'event\', \'send call no\', \'send\', \'email\');</script>';
	}
	else {
		echo $errormsg;

	}
}

?>
</div>
</body>
</html>