function showError(type, field, text)
{
  // ga('localsiteTracker.send', 'event', 'home-search-widget', 'error', eventLabel );
  var errorDiv = document.createElement('div');
  errorDiv.setAttribute('class', 'errortext');
  field.className += ' error';
  var sendForm;
  if (type === sms)
  {
    sendForm = document.getElementById('sendform-sms');
  }
  else if (type === email)
  {
    sendForm = document.getElementById('sendform-email');
  }
  sendForm.appendChild(errorDiv);
  sendForm.insertBefore(errorDiv, sendForm.childNodes[0]);
  errorDiv.innerHTML = text;
  field.focus();
}

function validator(type)
{
  var msg = document.querySelector('.errortext');
  var errDiv = document.querySelector('.error');
  if (msg)
  {
    msg.parentNode.removeChild(msg);
    var classRestore = errDiv.className.replace(/error/, '');
    errDiv.setAttribute('class', classRestore);
  }
  var phoneRegx = /^\d{10}$/;
  var emailRegx = /^\S+@\S+\.\S+$/i;
  var phoneField = document.getElementById('sms-address');
  var carrier = document.getElementById('carrier-gateway');
  var emailField = document.getElementById('email-address');
  if (type === sms)
  {
    if (phoneField.value === '')
    {
      showError(sms, phoneField, 'Please enter your phone number');
      return false;
    }
    else if (phoneRegx.test(phoneField.value) === false)
    {
      showError(sms, phoneField, 'Enter your 10-digit phone number with no spaces or punctuation');
      return false;
    }
    else if (carrier.value === '')
    {
      showError(sms, carrier, 'Please select your wireless carrier');
      return false;
    }
    else
    {
      return true;
    }
  }
  else if (type === email)
  {
    if (emailRegx.test(emailField.value) === false)
    {
      showError(email, emailField, 'Please enter a valid email address.');
      return false;
    }
    else
    {
      return true;
    }
  }
  else
  {
    return true;
  }
}